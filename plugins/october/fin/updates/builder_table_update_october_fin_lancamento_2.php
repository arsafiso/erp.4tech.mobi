<?php namespace october\fin\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberFinLancamento2 extends Migration
{
    public function up()
    {
        Schema::table('october_fin_lancamento', function($table)
        {
            $table->integer('ficha_cadastro_id')->nullable()->unsigned();
            $table->integer('ccusto_id')->nullable()->unsigned();
            $table->integer('tipo_doc_id')->nullable()->unsigned();
            $table->integer('caixa_id')->nullable()->unsigned();
            $table->integer('forma_pgto_id')->nullable()->unsigned();
            $table->integer('cond_pgto_id')->nullable()->unsigned();
            $table->integer('movimento_id')->nullable()->unsigned();
            $table->dropColumn('idficha_cadastro');
            $table->dropColumn('idccusto');
            $table->dropColumn('idtipo_doc');
            $table->dropColumn('idcaixa');
            $table->dropColumn('idforma_pgto');
            $table->dropColumn('idcond_pgto');
            $table->dropColumn('idmovimento');
        });
    }
    
    public function down()
    {
        Schema::table('october_fin_lancamento', function($table)
        {
            $table->dropColumn('ficha_cadastro_id');
            $table->dropColumn('ccusto_id');
            $table->dropColumn('tipo_doc_id');
            $table->dropColumn('caixa_id');
            $table->dropColumn('forma_pgto_id');
            $table->dropColumn('cond_pgto_id');
            $table->dropColumn('movimento_id');
            $table->integer('idficha_cadastro')->nullable()->unsigned();
            $table->integer('idccusto')->nullable()->unsigned();
            $table->integer('idtipo_doc')->nullable()->unsigned();
            $table->integer('idcaixa')->nullable()->unsigned();
            $table->integer('idforma_pgto')->nullable()->unsigned();
            $table->integer('idcond_pgto')->nullable()->unsigned();
            $table->integer('idmovimento')->nullable()->unsigned();
        });
    }
}
