<?php namespace october\fin\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberFinSaldoBancario extends Migration
{
    public function up()
    {
        Schema::table('october_fin_saldo_bancario', function($table)
        {
            $table->integer('pagrec')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('october_fin_saldo_bancario', function($table)
        {
            $table->dropColumn('pagrec');
        });
    }
}
