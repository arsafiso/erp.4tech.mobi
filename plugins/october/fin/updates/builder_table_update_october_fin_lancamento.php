<?php namespace october\fin\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberFinLancamento extends Migration
{
    public function up()
    {
        Schema::table('october_fin_lancamento', function($table)
        {
            $table->renameColumn('idtipodoc', 'idtipo_doc');
        });
    }
    
    public function down()
    {
        Schema::table('october_fin_lancamento', function($table)
        {
            $table->renameColumn('idtipo_doc', 'idtipodoc');
        });
    }
}
