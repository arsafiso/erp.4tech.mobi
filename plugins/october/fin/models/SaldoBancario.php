<?php namespace october\fin\Models;

use Model;

/**
 * Model
 */
class SaldoBancario extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'october_fin_saldo_bancario';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [        
        'caixa' => Caixa::class,
    ];

    public function listExtendQuery($query)
    {
        return $query->orderBy('id', 'desc');
    }

    public function getCaixaIdOptions()
    {
        return Caixa::lists('nome', 'id');
    }

    public function afterDelete() {
        $caixa = Caixa::where('id', $this->caixa_id)->first();
        
        if($this->pagrec == 1){
            $caixa->saldo = ( $this->valor_atual + $this->valor_movimentado );
        }else{
            $caixa->saldo = ( $this->valor_atual - $this->valor_movimentado );
        }
        $caixa->save();
    }
}
