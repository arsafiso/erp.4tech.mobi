<?php namespace october\fin\Models;

use Model;

/**
 * Model
 */
class CentroCusto extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'october_fin_ccusto';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public function getIdPaiOptions(){
        
        $pai = self::get();
        if($pai->count() > 0){            
            return self::lists('nome', 'id');
        }
        
        return [];        
    }
}
