<?php namespace october\fin\Models;

use Model;

/**
 * Model
 */
class CondicaoPagamento extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'october_fin_cond_pgto';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
