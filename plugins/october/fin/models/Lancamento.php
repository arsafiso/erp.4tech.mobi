<?php namespace october\fin\Models;

use Model;
use October\Glo\Models\FichaCadastro;

/**
 * Model
 */
class Lancamento extends Model
{
    use \October\Rain\Database\Traits\Validation;    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'october_fin_lancamento';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'ficha_cadastro' => 'October\Glo\Models\FichaCadastro',
        'ccusto' => CentroCusto::class,
        'formapgto' => FormaPagamento::class,
        'condpgto' => CondicaoPagamento::class,
        'tipodoc' => TipoDocumento::class,
        'caixa' => Caixa::class,
    ];

    public function getFichaCadastroOptions()
    {
        return FichaCadastro::lists('nome', 'id');
    }

    public function getCcustoOptions()
    {
        return CentroCusto::lists('nome', 'id');
    }
    
    public function getFormaPgtoIdOptions()
    {
        return FormaPagamento::lists('nome', 'id');
    }

    public function getCondPgtoIdOptions()
    {
        return CondicaoPagamento::lists('nome', 'id');
    }

    public function getTipoDocIdOptions()
    {
        return TipoDocumento::lists('nome', 'id');
    }
    
    public function getCaixaIdOptions()
    {
        return Caixa::lists('nome', 'id');
    }

    public function afterSave()
    {
        if($this->valor_liquido){ //  && $this->status == 2
        
            $lancamento = SaldoBancario::where('lancamento_id', $this->id)->first();
            
            $saldo = SaldoBancario::orderBy('id', 'desc')->first();
            if(!$lancamento){
                $fin = new SaldoBancario;
                $fin->lancamento_id = $this->id;
                $fin->caixa_id = $this->caixa_id;
                $fin->valor_movimentado = $this->valor_liquido;
                $fin->data = $this->data_baixa;
                $fin->pagrec = $this->pagrec; 
                $valor_saldo = $saldo->valor_atual ?? 0;
                if($this->pagrec == 1){
                    $fin->valor_atual = $valor_saldo - ($this->valor_liquido ?? 0);
                }else{
                    $fin->valor_atual = $valor_saldo + ($this->valor_liquido ?? 0);
                }        
                $fin->save();

                $caixa = Caixa::where('id', $this->caixa_id)->first();
                $caixa->saldo = $fin->valor_atual;
                $caixa->save();

            } //Só vai atualizar se o valor tiver sido alterado
            else if ($lancamento->valor_movimentado != $this->valor_liquido){            
                
                //só vai alterar se ainda não houve outra movimentação financeiro posteriormente
                if($lancamento->id == $saldo->id)
                {
                    //se a caixa tiver sido alterada
                    if($saldo->caixa_id != $this->caixa_id){
                        $saldo_caixa = SaldoBancario::where('caixa_id', $this->caixa_id)->orderBy('id', 'desc')->first();
                        $valor_saldo = $saldo_caixa->valor_atual ?? 0;
                    }else{
                        $ultimo_saldo = SaldoBancario::where('caixa_id', $this->caixa_id)->where('id', '<>', $saldo->id)->orderBy('id', 'desc')->first();
                        $valor_saldo = $ultimo_saldo->valor_atual ?? 0;
                    }

                    $saldo->caixa_id = $this->caixa_id;
                    $saldo->valor_movimentado = $this->valor_liquido;
                    $saldo->data = $this->data_baixa;
                    $saldo->pagrec = $this->pagrec;

                    if($this->pagrec == 1){
                        $saldo->valor_atual = $valor_saldo - $this->valor_liquido;
                    }else{
                        $saldo->valor_atual = $valor_saldo + $this->valor_liquido;
                    }
                    $saldo->save();

                    $caixa = Caixa::where('id', $this->caixa_id)->first();
                    $caixa->saldo = $saldo->valor_atual;
                    $caixa->save();
                }
            }
        }
    }

    public function afterDelete() {        
        SaldoBancario::where('lancamento_id', $this->id)->first()->delete();
    }
}