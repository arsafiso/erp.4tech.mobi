<?php namespace october\fin\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class CondicaoPagamentos extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'fin_forma_pgto', 
        'fin_cond_pgto' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('october.fin', 'main-menu-item', 'side-menu-item2');
    }
}
