<?php namespace october\fin\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class LancamentosReceber extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'fin_lancamento' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('october.fin', 'main-menu-item', 'side-menu-item6');
    }

    public function formBeforeCreate($model)
    {
        $model->pagrec = 2;
    }
}
