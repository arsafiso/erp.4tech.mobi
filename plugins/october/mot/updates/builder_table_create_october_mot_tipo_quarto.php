<?php namespace october\mot\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateOctoberMotTipoQuarto extends Migration
{
    public function up()
    {
        Schema::create('october_mot_tipo_quarto', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('idempresa')->unsigned()->default(1);
            $table->integer('idfilial')->unsigned()->default(1);
            $table->integer('idusuario_cria')->nullable()->unsigned();
            $table->integer('idusuario_alt')->nullable()->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('nome', 30)->nullable();
            $table->text('valor')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('october_mot_tipo_quarto');
    }
}
