<?php namespace october\mot\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberMotQuarto2 extends Migration
{
    public function up()
    {
        Schema::table('october_mot_quarto', function($table)
        {
            $table->integer('status')->nullable()->unsigned();
            $table->text('observacao')->nullable();
            $table->dropColumn('nome');
            $table->dropColumn('valor');
            $table->dropColumn('entrada');
            $table->dropColumn('saida');
        });
    }
    
    public function down()
    {
        Schema::table('october_mot_quarto', function($table)
        {
            $table->dropColumn('status');
            $table->dropColumn('observacao');
            $table->string('nome', 30)->nullable();
            $table->decimal('valor', 10, 2)->nullable();
            $table->dateTime('entrada')->nullable();
            $table->dateTime('saida')->nullable();
        });
    }
}
