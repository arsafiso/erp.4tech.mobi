<?php namespace october\mot\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberMotTipoQuarto extends Migration
{
    public function up()
    {
        Schema::table('october_mot_tipo_quarto', function($table)
        {
            $table->decimal('valor_padrao', 10, 2)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('october_mot_tipo_quarto', function($table)
        {
            $table->dropColumn('valor_padrao');
        });
    }
}
