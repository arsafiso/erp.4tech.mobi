<?php namespace october\mot\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberMotReserva4 extends Migration
{
    public function up()
    {
        Schema::table('october_mot_reserva', function($table)
        {
            $table->decimal('valor_produtos', 10, 2)->nullable();
            $table->decimal('valor_estadia', 10, 2)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('october_mot_reserva', function($table)
        {
            $table->dropColumn('valor_produtos');
            $table->dropColumn('valor_estadia');
        });
    }
}
