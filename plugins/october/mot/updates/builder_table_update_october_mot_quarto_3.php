<?php namespace october\mot\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberMotQuarto3 extends Migration
{
    public function up()
    {
        Schema::table('october_mot_quarto', function($table)
        {
            $table->renameColumn('idtipo_quarto', 'tipo_quarto_id');
        });
    }
    
    public function down()
    {
        Schema::table('october_mot_quarto', function($table)
        {
            $table->renameColumn('tipo_quarto_id', 'idtipo_quarto');
        });
    }
}
