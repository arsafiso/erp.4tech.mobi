<?php namespace october\mot\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberMotReserva2 extends Migration
{
    public function up()
    {
        Schema::table('october_mot_reserva', function($table)
        {
            $table->renameColumn('idquarto', 'quarto_id');
        });
    }
    
    public function down()
    {
        Schema::table('october_mot_reserva', function($table)
        {
            $table->renameColumn('quarto_id', 'idquarto');
        });
    }
}
