<?php namespace october\mot\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberMotReserva3 extends Migration
{
    public function up()
    {
        Schema::table('october_mot_reserva', function($table)
        {
            $table->integer('forma_pgto_id')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('october_mot_reserva', function($table)
        {
            $table->dropColumn('forma_pgto_id');
        });
    }
}
