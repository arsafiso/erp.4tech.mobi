<?php namespace october\mot\Models;

use Model;

/**
 * Model
 */
class Quarto extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'october_mot_quarto';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [    
        'tipo_quarto' => TipoQuarto::class,
    ];

    public function getTipoQuartoOptions()
    {
        return TipoQuarto::lists('nome', 'id');
    }
}