<?php namespace october\mot\Models;

use Db;
use Model;
use October\Fin\Models\Lancamento;
use October\Fin\Models\FormaPagamento;
use October\Cfe\Models\Produto;
use October\Cfe\Models\Movimento;
use October\Glo\Models\Calendario;

/**
 * Model
 */
class Reserva extends Model
{
    use \October\Rain\Database\Traits\Validation;    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    public $jsonable = ['produto'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'october_mot_reserva';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'quarto' => Quarto::class,
    ];
    
    public function beforeSave(){
        if($this->entrada && $this->saida){
            $entrada = date("Y-m-d", strtotime($this->entrada));
            $calendario = Calendario::where('data', $entrada)->where('tipo_data', 1);
            
            //verifico qual é o dia da semana
            if($calendario){
                $dia = 7;
            }else{
                $dia = date('w', $entrada);
            }

            $quarto = Quarto::join('october_mot_tipo_quarto', 'october_mot_quarto.tipo_quarto_id', '=', 'october_mot_tipo_quarto.id')
            ->select('october_mot_tipo_quarto.valor', 'valor_hora_padrao', 'valor_2horas_padrao', 'valor_pernoite_padrao')
            ->where('october_mot_quarto.id', $this->quarto_id)->first();

            $tempo = ((strtotime($this->saida) - strtotime($this->entrada)) / 60);

            if($quarto){
                
                if($quarto->valor){
                    $valor = json_decode($quarto->valor);               
    
                    foreach($valor as $array){                
                        $array = json_decode(json_encode($array), True);
                        //verifica se existe preço cadastrado para este dia da semana                
                        if($array['dia'] == $dia){
                            //verifica se existe preço cadastrado para este horário
                            if(strtotime($array['hora_inicio']) <= strtotime($this->entrada) && strtotime($array['hora_fim']) >= strtotime($this->entrada)){
                                if($tempo <= 60) {
                                    $preco_prio =  $array['valor_hora'];
                                }else if($tempo > 60 && $tempo <= 120) {
                                    $preco_prio =  $array['valor_2_horas'];
                                }else if($tempo > 120 && $tempo <= 180) {
                                    $preco_prio =  ($array['valor_2_horas'] + $array['valor_hora']);
                                }else{
                                    $preco_prio =  $array['valor_pernoite'];
                                }
                            }else{ //caso não tenha horário definido mas tenha o dia da semana cadastrado
                                if($tempo <= 60) {
                                    $preco =  $array['valor_hora'];
                                }else if($tempo > 60 && $tempo <= 120) {
                                    $preco =  $array['valor_2_horas'];
                                }else if($tempo > 120 && $tempo <= 180) {
                                    $preco =  ($array['valor_2_horas'] + $array['valor_hora']);
                                }else{
                                    $preco =  $array['valor_pernoite'];
                                }
                            }
                        }                
                    }
                }
            
                if(isset($preco_prio)){
                    $this->valor_estadia = $preco_prio;
                }else if(isset($preco)){
                    $this->valor_estadia = $preco;
                }else{
                    if($tempo <= 60) {
                        $this->valor_estadia =  $quarto->valor_hora_padrao;
                    }else if($tempo > 60 && $tempo <= 120) {
                        $this->valor_estadia =  $quarto->valor_2horas_padrao;
                    }else if($tempo > 120 && $tempo <= 180) {
                        $this->valor_estadia =  ($quarto->valor_2horas_padrao + $quarto->valor_hora_padrao);
                    }else{
                        $this->valor_estadia =  $quarto->valor_pernoite_padrao;
                    }
                }
            }            
        }

        
            
        $json_itens = [];
        $valor_produtos = 0;
        if($this->produto){
            $i = 0;
            foreach($this->produto as $pro){
                $produto = Produto::where('id', $pro['produto'])->first();
                $valor_produtos += $produto->preco_venda * ($pro['quantidade'] == '' ? 0 : $pro['quantidade']);
                $json_itens[$i]['produtos'] = $pro['produto'];
                $json_itens[$i]['quantidade'] = $pro['quantidade'];
                $json_itens[$i]['valor_unit'] = $produto->preco_venda;
                $i++;
            }            
        }
        $this->valor_produtos = $valor_produtos;
        $this->movimentaEstoque($this, $json_itens);

        $this->valor = intval($this->valor_produtos ?? 0) + intval($this->valor_estadia ?? 0);
    }

    public function afterSave()
    {
        $fin = Lancamento::where('tabela_origem', 'mot_reserva')->where('tabela_origem_id', $this->id)->first();
        if(!$fin){
            $fin = new Lancamento;
            $fin->tabela_origem = 'mot_reserva';
            $fin->tabela_origem_id = $this->id;
            $fin->data_emissao = $this->created_at;
            $fin->data_vencimento = $this->created_at;
            $fin->data_baixa = $this->created_at;
            $fin->status = 2;
            $fin->pagrec = 2;
            $fin->caixa_id = 1;
            $fin->cond_pgto_id = 1;
            $fin->forma_pgto_id = $this->forma_pgto_id;
            $fin->descricao = 'Títudo originado de Estadia';
        }
        $fin->valor_original = $this->valor;
        $fin->valor_liquido = $this->valor;
        $fin->save();
    }

    public function getQuartoOptions()
    {   
        return Quarto::join('october_mot_tipo_quarto', 'october_mot_quarto.tipo_quarto_id', '=', 'october_mot_tipo_quarto.id')
        ->select(DB::raw("CONCAT(october_mot_quarto.id, ' - ', october_mot_tipo_quarto.nome) as quarto, october_mot_quarto.id as id_quarto"))
        ->where('status', 1)
        ->lists('quarto', 'id_quarto');
    }

    public function getProdutoOptions()
    {
        return Produto::lists('nome', 'id');
    }

    public function getFormaPgtoIdOptions()
    {
        return FormaPagamento::lists('nome', 'id');
    }
    
    public function time2seconds($time='00:00:00')
    {
        list($hours, $mins, $secs) = explode(':', $time);
        return ($hours * 3600 ) + ($mins * 60 ) + $secs;
    }

    public function movimentaEstoque($obj, $json){        
        
        $mov = Movimento::where('tabela_origem', 'mot_reserva')->where('tabela_origem_id', $obj->id)->first();
        
        if(!empty($json)){
            if(!$mov){
                $mov = new Movimento;
                $mov->tabela_origem = 'mot_reserva';
                $mov->tabela_origem_id = $obj->id;
                $mov->data_emissao = $obj->created_at;
                $mov->data_vencimento = $obj->created_at;
                $mov->status = 1;
                $mov->pagrec = 2;
                $mov->tipo_mov_id = 2;
                $mov->local_estoque_id = 1;
                $mov->cond_pgto_id = 1;            
                $mov->descricao = 'Produtos consumidos na estadia.';
            }
            $mov->forma_pgto_id = $obj->forma_pgto_id;
            $mov->valor = $obj->valor_produtos;
            $mov->produtos = $json;
            $mov->save();
        }else if($mov){
            $mov->delete();
        }
    }

    public function afterDelete() {
        $mov = Movimento::where('tabela_origem', 'mot_reserva')->where('tabela_origem_id', $this->id)->get();
        if($mov->count() > 0){
            foreach($mov as $mov_prod){
                $mov_prod->delete();
            }
        }
    }
}