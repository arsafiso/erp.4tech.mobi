<?php namespace october\glo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateOctoberGloFichaCadastro extends Migration
{
    public function up()
    {
        Schema::create('october_glo_ficha_cadastro', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('idempresa')->unsigned()->default(1);
            $table->integer('idfilial')->unsigned()->default(1);
            $table->integer('idusuario_cria')->nullable()->unsigned();
            $table->integer('idusuario_alt')->nullable()->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('nome', 100)->nullable();
            $table->string('cpf_cnpj', 20)->nullable();
            $table->string('cep', 10)->nullable();
            $table->string('rua', 50)->nullable();
            $table->string('numero', 10)->nullable();
            $table->string('bairro', 20)->nullable();
            $table->string('complemento', 30)->nullable();
            $table->string('cidade', 30)->nullable();
            $table->string('uf', 2)->nullable();
            $table->string('telefone', 15)->nullable();
            $table->string('celular', 15)->nullable();
            $table->string('email', 50)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('october_glo_ficha_cadastro');
    }
}
