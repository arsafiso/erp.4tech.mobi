<?php namespace october\cfe\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberCfeBalancoEstoque extends Migration
{
    public function up()
    {
        Schema::table('october_cfe_balanco_estoque', function($table)
        {
            $table->integer('pagrec')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('october_cfe_balanco_estoque', function($table)
        {
            $table->dropColumn('pagrec');
        });
    }
}
