<?php namespace october\cfe\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberCfeMovimento3 extends Migration
{
    public function up()
    {
        Schema::table('october_cfe_movimento', function($table)
        {
            $table->integer('cond_pgto_id')->nullable()->unsigned();
            $table->integer('forma_pgto_id')->nullable()->unsigned();
            $table->integer('ficha_cadastro_id')->nullable()->unsigned();
            $table->integer('caixa_id')->nullable()->unsigned();
            $table->integer('local_estoque_id')->nullable()->unsigned();
            $table->integer('tipo_mov_id')->nullable()->unsigned();
            $table->dropColumn('idcond_pgto');
            $table->dropColumn('idforma_pgto');
            $table->dropColumn('idficha_cadastro');
            $table->dropColumn('idcaixa');
            $table->dropColumn('idlocal_estoque');
            $table->dropColumn('idtipo_mov');
        });
    }
    
    public function down()
    {
        Schema::table('october_cfe_movimento', function($table)
        {
            $table->dropColumn('cond_pgto_id');
            $table->dropColumn('forma_pgto_id');
            $table->dropColumn('ficha_cadastro_id');
            $table->dropColumn('caixa_id');
            $table->dropColumn('local_estoque_id');
            $table->dropColumn('tipo_mov_id');
            $table->integer('idcond_pgto')->nullable()->unsigned();
            $table->integer('idforma_pgto')->nullable()->unsigned();
            $table->integer('idficha_cadastro')->nullable()->unsigned();
            $table->integer('idcaixa')->nullable()->unsigned();
            $table->integer('idlocal_estoque')->nullable()->unsigned();
            $table->integer('idtipo_mov')->nullable()->unsigned();
        });
    }
}
