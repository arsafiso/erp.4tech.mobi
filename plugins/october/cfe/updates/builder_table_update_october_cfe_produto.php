<?php namespace october\cfe\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberCfeProduto extends Migration
{
    public function up()
    {
        Schema::table('october_cfe_produto', function($table)
        {
            $table->decimal('preco_custo', 10, 2)->nullable();
            $table->decimal('preco_venda', 10, 2)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('october_cfe_produto', function($table)
        {
            $table->dropColumn('preco_custo');
            $table->dropColumn('preco_venda');
        });
    }
}
