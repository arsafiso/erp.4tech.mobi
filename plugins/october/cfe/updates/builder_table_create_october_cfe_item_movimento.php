<?php namespace october\cfe\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateOctoberCfeItemMovimento extends Migration
{
    public function up()
    {
        Schema::create('october_cfe_item_movimento', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('movimento_id')->unsigned();
            $table->integer('produto_id')->nullable()->unsigned();
            $table->decimal('quantidade', 10, 2)->nullable();
            $table->decimal('valor_unit', 10, 2)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('october_cfe_item_movimento');
    }
}
