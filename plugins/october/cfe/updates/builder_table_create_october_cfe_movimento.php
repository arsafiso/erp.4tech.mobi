<?php namespace october\cfe\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateOctoberCfeMovimento extends Migration
{
    public function up()
    {
        Schema::create('october_cfe_movimento', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('id')->unsigned();
            $table->integer('idempresa')->unsigned()->default(1);
            $table->integer('idfilial')->unsigned()->default(1);
            $table->integer('idusuario_cria')->nullable()->unsigned();
            $table->integer('idusuario_alt')->nullable()->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->date('data_emissao')->nullable();
            $table->date('data_vencimento')->nullable();
            $table->integer('idcond_pgto')->nullable()->unsigned();
            $table->integer('idforma_pgto')->nullable()->unsigned();
            $table->integer('idficha_cadastro')->nullable()->unsigned();
            $table->integer('idcaixa')->nullable()->unsigned();
            $table->decimal('valor', 10, 2)->nullable();
            $table->integer('pagrec')->nullable()->unsigned();
            $table->integer('status')->nullable()->unsigned();
            $table->integer('numero')->nullable()->unsigned();
            $table->text('descricao')->nullable();
            $table->integer('idlocal_estoque')->nullable()->unsigned();
            $table->text('produtos')->nullable();
            $table->primary(['id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('october_cfe_movimento');
    }
}
