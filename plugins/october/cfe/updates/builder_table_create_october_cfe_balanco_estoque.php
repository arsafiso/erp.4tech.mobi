<?php namespace october\cfe\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateOctoberCfeBalancoEstoque extends Migration
{
    public function up()
    {
        Schema::create('october_cfe_balanco_estoque', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('idempresa')->unsigned()->default(1);
            $table->integer('idfilial')->unsigned()->default(1);
            $table->integer('idusuario_cria')->nullable();
            $table->integer('idusuario_alt')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('local_estoque_id')->nullable();
            $table->integer('produto_id')->nullable();
            $table->decimal('qtd_atual', 10, 2)->nullable();
            $table->decimal('qtd_movimentada', 10, 2)->nullable();
            $table->integer('movimento_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('october_cfe_balanco_estoque');
    }
}
