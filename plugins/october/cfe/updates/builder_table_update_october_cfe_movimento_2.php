<?php namespace october\cfe\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberCfeMovimento2 extends Migration
{
    public function up()
    {
        Schema::table('october_cfe_movimento', function($table)
        {
            $table->increments('id')->change();
        });
    }
    
    public function down()
    {
        Schema::table('october_cfe_movimento', function($table)
        {
            $table->integer('id')->change();
        });
    }
}
