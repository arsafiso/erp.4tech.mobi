<?php namespace october\cfe\Models;

use Model;

/**
 * Model
 */
class BalancoEstoque extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'october_cfe_balanco_estoque';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    
    public $belongsTo = [
        'local_estoque' => LocalEstoque::class,
        'produto' => Produto::class,
        'movimento' => Movimento::class,
    ];

    public function getProdutoOptions()
    {
        return Produto::lists('nome', 'id');
    }

    public function getLocalEstoqueOptions()
    {
        return LocalEstoque::lists('nome', 'id');
    }

    public function afterDelete() {
        $produto = Produto::where('id', $this->produto_id)->first();
        
        if($this->pagrec == 1){
            $produto->qtd_atual = ( $this->qtd_atual - $this->qtd_movimentada );
        }else{
            $produto->qtd_atual = ( $this->qtd_atual + $this->qtd_movimentada );
        }
        $produto->save();
    }
}
