<?php namespace october\cfe\Models;

use Model;
use October\Glo\Models\FichaCadastro;
use October\Fin\Models\CentroCusto;
use October\Fin\Models\FormaPagamento;
use October\Fin\Models\CondicaoPagamento;
use October\Fin\Models\TipoDocumento;
use October\Fin\Models\Caixa;
use October\Fin\Models\Lancamento;
/**
 * Model
 */
class Movimento extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    public $jsonable = ['produtos'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'october_cfe_movimento';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'ficha_cadastro' => 'October\Glo\Models\FichaCadastro',
        'ccusto' => CentroCusto::class,
        'forma_pgto' => FormaPagamento::class,
        'cond_pgto' => CondicaoPagamento::class,
        'tipo_doc' => TipoDocumento::class,
        'caixa' => Caixa::class,
        'local_estoque' => LocalEstoque::class,
        'tipo_mov' => TipoMovimento::class,
    ];

    public function getFichaCadastroOptions()
    {
        return FichaCadastro::lists('nome', 'id');
    }

    public function getCcustoOptions()
    {
        return CentroCusto::lists('nome', 'id');
    }
    
    public function getFormaPgtoOptions()
    {
        return FormaPagamento::lists('nome', 'id');
    }

    public function getCondPgtoOptions()
    {
        return CondicaoPagamento::lists('nome', 'id');
    }

    public function getTipodocOptions()
    {
        return TipoDocumento::lists('nome', 'id');
    }
    
    public function getCaixaOptions()
    {
        return Caixa::lists('nome', 'id');
    }

    public function getTipoMovOptions()
    {
        return TipoMovimento::lists('nome', 'id');
    }

    public function getLocalEstoqueOptions()
    {
        return LocalEstoque::lists('nome', 'id');
    }

    public function getProdutosOptions()
    {
        return Produto::lists('nome', 'id');
    }

    public function beforeSave()
    {
        if($this->produtos != '[]'){
            $valor = 0;
            foreach($this->produtos as $prod){
                if($prod['quantidade'] > 0 && $prod['valor_unit'] > 0)
                {
                    $valor = $valor + (floatval($prod['quantidade']) * floatval($prod['valor_unit']));
                }
            }
            $this->valor = $valor;
        }
    }

    public function afterSave()
    {
        $tipoMov = TipoMovimento::where('id', $this->tipo_mov_id)->first();
        
        if($tipoMov->mov_estoque)
        {
            if($this->produtos != '[]')
            {                
                $itens = [];
                foreach($this->produtos as $prod)
                {
                    if($prod['produtos'] > 0 && $prod['quantidade'] > 0)
                    {
                        $item = ItemMovimento::where('movimento_id', $this->id)->where('produto_id', $prod['produtos'])->first();
                        if(!$item){
                            $item = new ItemMovimento;
                            $item->produto_id = $prod['produtos'];
                            $item->movimento_id = $this->id;
                        }
                        $item->quantidade = $prod['quantidade'];
                        $item->valor_unit = $prod['valor_unit'];
                        $item->save();
                        $itens[] = $prod['produtos'];

                        $estoque = BalancoEstoque::where('produto_id', $prod['produtos'])
                            ->where('local_estoque_id', $this->local_estoque_id)
                            ->where('movimento_id', $this->id)->first();
                        
                        if (!$estoque || $estoque->qtd_movimentada != floatval($prod['quantidade'])){
                            
                            $produto = Produto::where('id', $prod['produtos'])->first();

                            if(!$estoque){
                                $estoque = new BalancoEstoque;
                                $estoque->local_estoque_id = $this->local_estoque_id;
                                $estoque->movimento_id = $this->id;
                                $estoque->produto_id = $prod['produtos'];
                                $estoque->pagrec = $this->pagrec;
                                //se nunca teve estoque, vamos pegar a quantidade atual cadastrada.
                                $estoque_atual = ($produto->qtd_atual ?? 0);
                            }else{
                                // se já teve movimentação de estoque, vamos pegar a quantidade anterior a movimentação atual.
                                if($this->pagrec == 1){
                                    $estoque_atual = ($estoque->qtd_atual - $estoque->qtd_movimentada);
                                }else{
                                    $estoque_atual = ($estoque->qtd_atual + $estoque->qtd_movimentada);
                                }
                            }

                            if($this->pagrec == 1){
                                $estoque->qtd_atual = floatval($estoque_atual) + floatval($prod['quantidade']);
                            }else{
                                $estoque->qtd_atual = floatval($estoque_atual) - floatval($prod['quantidade']);
                            }

                            $produto->qtd_atual = $estoque->qtd_atual;
                            $produto->save();

                            $estoque->qtd_movimentada = floatval($prod['quantidade']);
                            $estoque->save();
                        }
                    }
                }
                $item = ItemMovimento::where('movimento_id', $this->id)->whereNotIn('produto_id', $itens)->get();
                if($item->count() > 0){
                    foreach($item as $item_prod){
                        $item_prod->delete();
                    }
                }
            }
        }
        
        if($tipoMov->mov_financeiro){
            $fin = Lancamento::where('tabela_origem', 'cfe_movimento')->where('tabela_origem_id', $this->id)->first();
            if(!$fin){
                $fin = new Lancamento;
                $fin->tabela_origem = 'cfe_movimento';
                $fin->tabela_origem_id = $this->id;
                $fin->data_emissao = $this->data_emissao;
                $fin->data_vencimento = $this->data_vencimento;
                $fin->status = 2; //1 = aberto, 2 = pago
                $fin->pagrec = $this->pagrec;
                $fin->caixa_id = $this->caixa_id;
                $fin->cond_pgto_id = $this->cond_pgto_id;
            }
            $fin->valor_original = $this->valor;
            $fin->valor_liquido = $this->valor;
            $fin->save();
        }
    }

    public function afterDelete() {
        
        $balanco = BalancoEstoque::where('movimento_id', $this->id)->get();
        foreach($balanco as $bal){
            $bal->delete();            
        }
        Lancamento::where('tabela_origem', 'cfe_movimento')->where('tabela_origem_id', $this->id)->delete();

        $item = ItemMovimento::where('movimento_id', $this->id)->get();
        if($item->count() > 0){
            foreach($item as $item_prod){
                $item_prod->delete();
            }
        }
    }    
}