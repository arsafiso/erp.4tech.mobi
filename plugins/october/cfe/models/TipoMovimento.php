<?php namespace october\cfe\Models;

use Model;

/**
 * Model
 */
class TipoMovimento extends Model
{
    use \October\Rain\Database\Traits\Validation;    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'october_cfe_tipo_movimento';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
