<?php namespace october\cfe\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class LocaisEstoque extends Controller
{
    public $implement = [       
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\ReorderController'
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'cfe_local_estoque' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('october.cfe', 'main-menu-item', 'side-menu-item2');
    }
}
