<?php namespace october\cfe\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class BalancoEstoque extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    
    public $requiredPermissions = [
        'cfe_balanco_estoque' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('october.cfe', 'main-menu-item', 'side-menu-item4');
    }

    public function listExtendQuery($query)
    {
        return $query->orderBy('id', 'desc');
    }
}
